#!/bin/bash
# Installs extra packages e.g. Chrome, Skype.

# Enable MultiArch for i386 packages (Skype).
dpkg --add-architecture i386

# Add repositories:
#   - Chrome (add key first)
wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -
#echo "deb http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google-chrome.list
add-apt-repository "deb http://dl.google.com/linux/chrome/deb/ stable main"
#   - Canonical Partner (Skype)
add-apt-repository "deb http://archive.canonical.com/ $(lsb_release -sc) partner"

# Update package lists.
apt-get update

# Install packages.
apt-get install --yes google-chrome-stable
apt-get install --yes skype
