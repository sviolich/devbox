#!/bin/bash
# Installs packages for development environment.

# Update package lists and existing packages.
apt-get update
apt-get upgrade --yes

# Set interactive install values:
#   - MySQL server root user password empty
debconf-set-selections <<< 'mysql-server mysql-server/root_password password ""'
debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password ""'

# Install packages.
apt-get install --yes dmenu
apt-get install --yes git
apt-get install --yes herbstluftwm
apt-get install --yes mysql-client
apt-get install --yes mysql-server
apt-get install --yes postgresql
apt-get install --yes python
apt-get install --yes python-pip
apt-get install --yes vim
apt-get install --yes virtualenvwrapper

# Install NeoBundle plugin manager for Vim. 
curl https://raw.githubusercontent.com/Shougo/neobundle.vim/master/bin/install.sh | bash

# Install Monaco font.
curl -kL https://raw.github.com/cstrap/monaco-font/master/install-font-ubuntu.sh | bash

# If on laptop, install laptop-specific packages.
if [[ `upower -e` =~ 'line_power_AC0' ]]; then
    apt-get install --yes acpi
    apt-get install --yes xbacklight
fi
